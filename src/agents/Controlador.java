package agents;

import ontologies.AmbientOntology;
import jade.content.lang.sl.SLCodec;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import behaviours.ControllerListener;


public class Controlador extends Agent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void setup() {
		super.setup();
		
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		
		sd.setName("Controlador");
		sd.setType("Regulador");
		
		dfd.addServices(sd);
		
		try {
			DFService.register(this, dfd);
		    }
	    catch (FIPAException fe) {
	    	fe.printStackTrace();
	    	}

		//Register the SL content language
		this.getContentManager().registerLanguage(new SLCodec(),"fipa-sl");
		//Register the mobility ontology
		this.getContentManager().registerOntology( AmbientOntology.getInstance());
		
		addBehaviour(new ControllerListener());
		
	}

	@Override
	protected void takeDown() {
		super.takeDown();
	}
	

}
