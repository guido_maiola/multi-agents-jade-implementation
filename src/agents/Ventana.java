package agents;

import ontologies.AmbientOntology;
import jade.content.lang.sl.SLCodec;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import behaviours.WChangeStatus;
import behaviours.WStatus;

public class Ventana extends Agent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean abierta = true;

	public String estado() {
		if (abierta)
			return "ABIERTA";
		else
			return "CERRADA";
	}

	public void cambiar() {
		
		String a = (abierta ? "abierta":"cerrada");
		System.out.println("Ventana: Estoy " + a);

		if (!this.abierta) { // Si esta cerrada
			abierta = true;
			a = (abierta ? "abierta":"cerrada");
			System.out.println("Ventana: Ahora estoy " + a);
		} else {
			abierta = false;
			a = (abierta ? "abierta":"cerrada");
			System.out.println("Ventana: Ahora estoy " + a);
		}

	}

	
	
	@Override
	protected void setup() {
		
		super.setup();
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();

		sd.setName("Ventana");
		sd.setType("Artefacto");

		dfd.addServices(sd);

		// Register the SL content language
		this.getContentManager().registerLanguage(new SLCodec(),"fipa-sl");

		// Register the mobility ontology
		getContentManager().registerOntology(AmbientOntology.getInstance());

		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}

		addBehaviour(new WChangeStatus());
		addBehaviour(new WStatus());
	}

	@Override
	protected void takeDown() {
		super.takeDown();
	}

}
