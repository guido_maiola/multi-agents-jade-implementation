package agents;

import jade.core.Agent;
import behaviours.TermostatoB;;


public class Termostato extends Agent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void setup() {
		super.setup();
		addBehaviour(new TermostatoB(this));
	}

	@Override
	protected void takeDown() {
		super.takeDown();
	}
	

}
