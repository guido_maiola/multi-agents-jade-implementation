package behaviours;

import jade.core.AID;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.SequentialBehaviour;

public class TermMessengerB extends SequentialBehaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	TermMessengerB(AID receiver, String cambioTemp) {

		DataStore ds = new DataStore();
		
		// Comport. que se ocupa de enviar Inform
		TermSendInformB sendInformB = new TermSendInformB(receiver, cambioTemp);
		// Comport. que se ocupa de recibir la Confirmacion/Desconf.
		TermRecConfB recConfB = new TermRecConfB();
		
		sendInformB.setDataStore(ds);
		recConfB.setDataStore(ds);

		this.addSubBehaviour(sendInformB);
		this.addSubBehaviour(recConfB);
	}

}
