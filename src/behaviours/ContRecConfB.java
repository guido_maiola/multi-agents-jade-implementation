package behaviours;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ContRecConfB extends Behaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7045494928898764865L;
	
	private boolean done = false;

	public ContRecConfB() {
	}

	@Override
	public void action() {

		// Recibo respuestas a los pedidos
		ACLMessage msg = myAgent.receive((MessageTemplate) getDataStore().get("KEY-MC"));

		if (msg != null) {
			
			switch (msg.getPerformative()) {

			case (ACLMessage.CONFIRM):
				System.out.println("Controlador (ContRecConfB): Llego la confirmacion de la ventana");
				done = true;
				break;
			case (ACLMessage.DISCONFIRM):
				System.out.println("Controlador (ContRecConfB): Llego la disconfirmacion de la ventana ");
				done = true;
				break;
			}
		} else {
			block();
		}

	}

	@Override
	public boolean done() {
		return done;
	}

}
