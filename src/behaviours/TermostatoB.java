package behaviours;

import jade.core.Agent;

import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;


public class TermostatoB extends TickerBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static int PERIODO = 10000; // AJUSTAR TEMPORIZADOR

	public TermostatoB(Agent a) {
		super(a, PERIODO); // Periodo en milisegundos
	}

	private String cambioTemp() {
		
		double temp = Math.random();
		double mitad = 0.5;
		String resultado;

		if (temp > mitad ) // Si subio
			resultado = "ALTA";
		else {
			if (temp < 0.5) { // Si bajo
				resultado = "BAJA";
			} else {
				resultado = "IGUAL";
			}
		}
		System.out.println("Nueva temperatura: " + resultado);
		return resultado;
	}

	@Override
	protected void onTick() {

		// Si la temperatura cambió le aviso al controlador
		String cambioTemp = cambioTemp();

		if (!("IGUAL".equals(cambioTemp))) {
			
			// Busco al servicio registrado como Controlador en el DF
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setName("Controlador");
			sd.setType("Regulador");
			template.addServices(sd);
			
			try {
				DFAgentDescription[] result = DFService.search(myAgent,template);
				
				// Itero el resultado
				for (int i = 0; i < result.length; i++) {
					
					// Instancio un comportamiento secuencial por cada controlador que tengo que notificar
					TermMessengerB termMessengerB = new TermMessengerB(result[i].getName(),cambioTemp);
					
					myAgent.addBehaviour(termMessengerB);

				}
				
			} catch (FIPAException fe) {
				fe.printStackTrace();
			}
		}

		

	}


}
