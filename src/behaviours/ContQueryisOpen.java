package behaviours;


import ontologies.AmbientOntology;
import ontologies.IsWindowClosePredicate;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ContQueryisOpen extends Behaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4292414926785666789L;
	
	private AID ventanaAID;
	private boolean done = false;

	ContQueryisOpen(AID receiver) {
		ventanaAID = receiver;
	}

	@Override
	public void action() {
		
		System.out.println("Controlador (ContQueryisOpen): Le pregunto a la ventana " + ventanaAID + " si esta abierta.");
		ACLMessage msg = new ACLMessage(ACLMessage.QUERY_IF);
		msg.addReceiver(ventanaAID);
		
		IsWindowClosePredicate p = new IsWindowClosePredicate();
		p.setWindowid(ventanaAID);
		p.setWindowstate("ABIERTA");
		
		msg.setOntology(AmbientOntology.ONTOLOGY_NAME);
		
		msg.setLanguage("fipa-sl");
		System.out.println("Codec language!:" + myAgent.getContentManager().lookupLanguage("fipa-sl").getName());

		try {
			myAgent.getContentManager().fillContent(msg, p);
		} catch (NullPointerException | CodecException | OntologyException e) {
			e.printStackTrace();
		}
		
		String convId = myAgent.getLocalName()+ System.currentTimeMillis();
		msg.setConversationId(convId);
		msg.setReplyWith(convId);
		
		myAgent.send(msg);
		
		getDataStore().put("KEY-ST", MessageTemplate.and(MessageTemplate.MatchConversationId(msg.getConversationId()),
				MessageTemplate.MatchInReplyTo(msg.getReplyWith())));
		done = true;
	}

	@Override
	public boolean done() {
		return done;
	}

}
