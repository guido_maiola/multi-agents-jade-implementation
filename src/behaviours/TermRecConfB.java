package behaviours;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class TermRecConfB extends Behaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean done = false;

	public TermRecConfB() {
	}

	@Override
	public void action() {

		// Recibo respuestas a los pedidos
		ACLMessage msg = myAgent.receive((MessageTemplate) getDataStore().get("KEY-MT"));

		if (msg != null) {
			if (msg.getPerformative() == ACLMessage.CONFIRM) {
				System.out.println("(Termostato): Llego la confirmacion de la conversacion con el Controlador");
				done  = true;
			}
		} else {
			block();
		}

	}

	@Override
	public boolean done() {
		return done;
	}

}
