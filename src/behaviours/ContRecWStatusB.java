package behaviours;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ContRecWStatusB extends Behaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2582093045382779135L;

	private String cambioTemp;
	private boolean done;
	private int state;

	ContRecWStatusB(String temp) {
		cambioTemp = temp;
		done = false;
	}

	public void action() {

		// Recibo la respuesta a la pregunta de status de la ventana
		ACLMessage msg = myAgent.receive((MessageTemplate) getDataStore().get("KEY-ST"));

		if (msg != null) {
			int msgPer = msg.getPerformative();

			switch (cambioTemp) {
			case "ALTA":
				if (msgPer == ACLMessage.DISCONFIRM)
					state = 1; // cambiar de estado
				else
					state = 0; // dejar igual
				break;
			case "BAJA":
				if (msgPer == ACLMessage.CONFIRM)
					state = 1; // cambiar de estado
				else
					state = 0; // dejar igual
				break;
			}
			done = true;
		}
	}

	@Override
	public boolean done() {
		return done;
	}

	@Override
	public int onEnd() {
		return state;
	}

}
