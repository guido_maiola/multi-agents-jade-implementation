package behaviours;

import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ControllerListener extends CyclicBehaviour {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3339516913184804801L;

	@Override
	public void action() {
		
		// Esta escuchando mensajes INFORM
//		ACLMessage msg = myAgent.receive(MessageTemplate.or(MessageTemplate.MatchPerformative(ACLMessage.INFORM), MessageTemplate.MatchSender()));
		ACLMessage msg = myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
				
		if (msg != null) {

			String cambioTemp = msg.getContent();
			System.out.println("Controlador(ControladorBehaviour): Me informan cambio de temperatura. La temperatura actual es mas " + cambioTemp + " que la anterior");
			
			ContConfB contConfB = new ContConfB(msg.createReply());
			myAgent.addBehaviour(contConfB);

	
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setName("Ventana");
			sd.setType("Artefacto");
			template.addServices(sd);
			try {
				// Un comp. secuencial por cada ventana, gestiona la mensajeria
				DFAgentDescription[] result = DFService.search(myAgent, template);
				for (int i = 0; i < result.length; i++) {
					ContWMessengerB contWMessengerB = new ContWMessengerB(result[i].getName(), cambioTemp);
					myAgent.addBehaviour(contWMessengerB);
				}

			} catch (FIPAException fe) {
				fe.printStackTrace();
			}
		}

	}

}
