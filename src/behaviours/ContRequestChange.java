package behaviours;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ContRequestChange extends Behaviour {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2936287312054238930L;
	
	private AID receiver;
	private boolean done = false;

	public ContRequestChange(AID r, String a) {
		receiver = r;
	}

	@Override
	public void action() {
		
		System.out.println("Controlador(ContRequestChange): Le pido a la ventana " + receiver.getName() + " cambiar de estado");
		
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.addReceiver(receiver);
		msg.setContent("CHANGE-STATUS");
		String convId = myAgent.getLocalName()+ System.currentTimeMillis();
		msg.setConversationId(convId);
		msg.setReplyWith(convId);
		
		myAgent.send(msg);
		
		getDataStore().put("KEY-MC", MessageTemplate.and(MessageTemplate.MatchConversationId(msg.getConversationId()),
				MessageTemplate.MatchInReplyTo(msg.getReplyWith())));
		done = true;
	}

	@Override
	public boolean done() {
		return done;
	}

}
