package behaviours;

import ontologies.IsWindowClosePredicate;
import agents.Ventana;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.OntologyException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class WStatus extends CyclicBehaviour {

	private static final long serialVersionUID = 1256671802209297862L;
	
	@Override
	public void action() {

		ACLMessage msg = myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.QUERY_IF));

		if (msg != null) {

			System.out.println("Ventana(WStatus): Le respondo al controlador con el estado de la ventana");
			
			String WState = null;
			
			//Extraer contenido
			try {
				IsWindowClosePredicate p = (IsWindowClosePredicate) myAgent.getContentManager().extractContent(msg);
				WState = p.getWindowstate();
				System.out.println("Estado de la ventana:" + WState);
				
				
			} catch (CodecException | OntologyException e) {
				e.printStackTrace();
			}
			
			
			Ventana v = (Ventana) myAgent;
			ACLMessage replyQ = msg.createReply();
			
			if (v.estado().equals(WState)) {
				replyQ.setPerformative(ACLMessage.CONFIRM);
			} else {
				replyQ.setPerformative(ACLMessage.DISCONFIRM);
			}
			myAgent.send(replyQ);
			
		}
	}


}
