package behaviours;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class TermSendInformB extends Behaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private AID controlerAID;
	String cambio;

	private boolean done = false;

	public TermSendInformB(AID caid,String cambioTemp) {
		controlerAID = caid;
		cambio = cambioTemp;
	}

	@Override
	public void action() {
		
		//Envio INFORM de cambio de temperatura
		System.out.println("Termostato: INFORM al controlador ");
		
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.addReceiver(controlerAID);
		msg.setContent(cambio);
		String convId = myAgent.getLocalName() + System.currentTimeMillis();
		msg.setConversationId(convId);
		msg.setReplyWith(convId);
		
		myAgent.send(msg);
		
		getDataStore().put("KEY-MT", MessageTemplate.and(MessageTemplate.MatchConversationId(msg.getConversationId()),
				MessageTemplate.MatchInReplyTo(msg.getReplyWith())));
		done = true;
	}

	
	public boolean done() {
		return done;
	}

}
