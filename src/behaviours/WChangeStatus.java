package behaviours;

import agents.Ventana;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class WChangeStatus extends CyclicBehaviour {

	private static final long serialVersionUID = 1009815605339810742L;
	

	@Override
	public void action() {
		
		ACLMessage msg = myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

		if (msg != null) {
			
			System.out.println("Ventana(WChangeStatus): Recibo el pedido de cambio de estado del controlador");

			Ventana v = (Ventana) myAgent;
			ACLMessage replyR = msg.createReply();
			// se cambia el estado de la ventana
			v.cambiar();
			replyR.setPerformative(ACLMessage.CONFIRM);
			System.out.println("Ventana(WChangeStatus): Envio la confirmacion de cambio de estado");
			myAgent.send(replyR);
		}
		
	}

}
