package behaviours;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class ContConfB extends Behaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8050284714944996132L;
	
	private ACLMessage reply;

	public ContConfB(ACLMessage r) {
		reply = r;
	}

	@Override
	public void action() {
		
		ACLMessage rep = reply;
		rep.setPerformative(ACLMessage.CONFIRM);
		myAgent.send(rep);

	}

	@Override
	public boolean done() {
		return true;
	}

}
