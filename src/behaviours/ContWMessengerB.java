package behaviours;

import jade.core.AID;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.FSMBehaviour;

public class ContWMessengerB extends FSMBehaviour {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2406802080524724152L;

	
	ContWMessengerB(AID receiver, String cambioTemp) {
		
		DataStore ds = new DataStore();

		// Comport. que pregunta el estado de la ventana (abierto o cerrado)
		ContQueryisOpen contQueryisOpen = new ContQueryisOpen(receiver);
		// Comport. que recibe la respuesta de estado de la ventana
		ContRecWStatusB recWStatusB = new ContRecWStatusB(cambioTemp);
		// Comport. que se ocupa de enviar Request
		ContRequestChange contRequestChange = new ContRequestChange(receiver, cambioTemp);
		// Comport. que se ocupa de recibir la confirmacion/desconf.
		ContRecConfB recConfB = new ContRecConfB();
	
		FinalState finalState = new FinalState();
	
		// registro todos los comportamientos (estados) + ultimo
		registerFirstState(contQueryisOpen, "query-status");
		registerState(recWStatusB, "rec-status");
		registerState(contRequestChange,"req-change");
		registerState(recConfB,"rec-conf");
		registerLastState(finalState,"fin");
		
		// registro las transiciones
		registerDefaultTransition("query-status", "rec-status");
		registerTransition("rec-status", "req-change", 1);
		registerDefaultTransition("req-change", "rec-conf");
		registerDefaultTransition("rec-conf", "fin");
		registerTransition("rec-status", "fin", 0);

		// seteo la datastore a todos los comportamientos
		contQueryisOpen.setDataStore(ds);
		recWStatusB.setDataStore(ds);
		contRequestChange.setDataStore(ds);
		recConfB.setDataStore(ds);
		
	}
}