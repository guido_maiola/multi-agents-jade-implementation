package ontologies;

import jade.content.Predicate;
import jade.core.AID;

public class IsWindowClosePredicate implements Predicate{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8518613716847250491L;
	
	AID windowid;
	String windowstate;
	
	public IsWindowClosePredicate() {
		
	}

	public AID getWindowid() {
		return windowid;
	}

	public void setWindowid(AID windowid) {
		this.windowid = windowid;
	}

	public String getWindowstate() {
		return windowstate;
	}

	public void setWindowstate(String windowstate) {
		this.windowstate = windowstate;
	}
	
	
	

}
